# -*- coding: utf-8 -*-
"""
Created on Thu Jan 30 15:00:51 2020

@author: Ujjawal.K.Panchal & Hardik M. Ajmani
"""
import tensorflow as tf
import pandas as pd
import os
import numpy as np
from keras import Sequential, Model
from keras.layers import Dense, Input, Flatten, Activation, Reshape
from keras.optimizers import Adam, RMSprop
from keras.layers import Conv1D, Conv2DTranspose, UpSampling2D
from keras.layers import LeakyReLU, Dropout
from keras.layers import BatchNormalization, Cropping2D
from keras.layers import Input, Dense, Reshape, Flatten, Dropout
from keras.layers import BatchNormalization, Activation, ZeroPadding2D, Cropping1D, Cropping2D
from keras.layers.advanced_activations import LeakyReLU
from keras.layers.convolutional import UpSampling2D, Conv2D
from keras.models import Sequential, Model
from keras.optimizers import RMSprop
import keras.backend as K
"""
X_curves_global = list()
X_curves_local = list()
X_tce_period = list()
Y = list()



filenames = os.listdir('tfrecord')
i = 0
for filename in filenames:
    for record in tf.python_io.tf_record_iterator('tfrecord/'+filename):
      ex = tf.train.Example.FromString(record)
      X_curves_global.append(np.array(ex.features.feature['global_view'].float_list.value, dtype = float))
      X_curves_local.append(np.array(ex.features.feature['local_view'].float_list.value, dtype = float))
      X_tce_period.append(ex.features.feature['tce_period'].float_list.value)
      Y.append(1 if ex.features.feature['av_training_set'].bytes_list.value[0] == b'PC' else 0)
      i+=1
      print(i)

Y = pd.DataFrame(Y)
X_curves_global = pd.DataFrame(X_curves_global)
X_curves_local = pd.DataFrame(X_curves_local)
X_tce_period = pd.DataFrame(X_tce_period)

X_curves_global.to_csv('X_curves_global.csv', header = False, sep = ',',  line_terminator = '\n', engine = 'python')
X_curves_local.to_csv('X_curves_local.csv', header = False, sep = ',', line_terminator = '\n')
X_tce_period.to_csv('X_tce_period.csv', header = False, sep = ',', line_terminator = '\n')
Y.to_csv('Y.csv', header = False, sep = ',', line_terminator = '\n')
    
X_curves_local = X_curves_local.values
Y = Y.values
"""
X = pd.read_csv("X_curves_local.csv", header = None).iloc[:,1:].values
Y = pd.read_csv('Y.csv', header = None).iloc[:,1:].values

from sklearn.model_selection import train_test_split
x_train, x_test, y_train, y_test = train_test_split(X, Y, test_size = 0.2, shuffle = True)


merged = pd.concat([pd.DataFrame(x_train), pd.DataFrame(y_train)], axis = 1)
merged.shape

pos_list = [x for x in merged.values if x[-1] == 1]
neg_list = [x for x in merged.values if x[-1] == 0]
print(len(pos_list), len(neg_list))

fin_dataset = list()
for i,x in enumerate(pos_list):
    if(i<300):
        fin_dataset.append(x)
fin_dataset.extend(neg_list)
fin_dataset = pd.DataFrame(fin_dataset)

x_train = fin_dataset.iloc[:,:-1].values
y_train = fin_dataset.iloc[:,-1].values

from sklearn.preprocessing import StandardScaler

sc = StandardScaler()
x_train = sc.fit_transform(x_train)
x_test = sc.transform(x_test)


class WGAN():
    def __init__(self):
        self.img_rows = 201
        self.img_cols = 1
        self.channels = 1
        self.img_shape = (self.img_rows, self.img_cols, self.channels)
        self.latent_dim = 100

        # Following parameter and optimizer set as recommended in paper
        self.n_critic = 5
        self.clip_value = 0.01
        optimizer = RMSprop(lr=0.00005)

        # Build and compile the critic
        self.critic = self.build_critic()
        self.critic.compile(loss=self.wasserstein_loss,
            optimizer=optimizer,
            metrics=['accuracy'])

        # Build the generator
        self.generator = self.build_generator()

        # The generator takes noise as input and generated imgs
        z = Input(shape=(self.latent_dim,))
        img = self.generator(z)

        # For the combined model we will only train the generator
        self.critic.trainable = False

        # The critic takes generated images as input and determines validity
        valid = self.critic(img)

        # The combined model  (stacked generator and critic)
        self.combined = Model(z, valid)
        self.combined.compile(loss=self.wasserstein_loss,
            optimizer=optimizer,
            metrics=['accuracy'])

    def wasserstein_loss(self, y_true, y_pred):
        return K.mean(y_true * y_pred)

    def build_generator(self):

        model = Sequential()

        model.add(Dense(201 * 1 * 1, activation="relu", input_dim = 100))
        model.add(Reshape((201, 1, 1)))
        model.add(UpSampling2D(size = (1, 1)))
        model.add(Conv2D(128, kernel_size=4, padding="same"))
        model.add(BatchNormalization(momentum=0.8))
        model.add(Activation("relu"))
        model.add(UpSampling2D(size = (1, 1)))
        model.add(Conv2D(64, kernel_size=4, padding="same"))
        model.add(BatchNormalization(momentum=0.8))
        model.add(Activation("relu"))
        model.add(Conv2D(1, kernel_size=4, padding="same"))
        #model.add(Cropping2D((1, 0)))
        model.add(Reshape((201, 1, 1)))
        model.add(Activation("tanh"))

        model.summary()

        noise = Input(shape=(self.latent_dim,))
        img = model(noise)

        return Model(noise, img)

    def build_critic(self):

        model = Sequential()

        model.add(Conv2D(16, kernel_size=3, strides=2, input_shape=self.img_shape, padding="same"))
        model.add(LeakyReLU(alpha=0.2))
        model.add(Dropout(0.25))
        model.add(Conv2D(32, kernel_size=3, strides=2, padding="same"))
        model.add(ZeroPadding2D(padding=((0,1),(0,1))))
        model.add(BatchNormalization(momentum=0.8))
        model.add(LeakyReLU(alpha=0.2))
        model.add(Dropout(0.25))
        model.add(Conv2D(64, kernel_size=3, strides=2, padding="same"))
        model.add(BatchNormalization(momentum=0.8))
        model.add(LeakyReLU(alpha=0.2))
        model.add(Dropout(0.25))
        model.add(Conv2D(128, kernel_size=3, strides=1, padding="same"))
        model.add(BatchNormalization(momentum=0.8))
        model.add(LeakyReLU(alpha=0.2))
        model.add(Dropout(0.25))
        model.add(Flatten())
        model.add(Dense(1))

        model.summary()

        img = Input(shape=self.img_shape)
        validity = model(img)

        return Model(img, validity)

    def train(self, epochs, batch_size=128, sample_interval=50):

        # Load the dataset
        #(X_train, _), (_, _) = mnist.load_data()
        X_train = data
        # Rescale -1 to 1
        #X_train = (X_train.astype(np.float32) - 127.5) / 127.5
        #X_train = np.expand_dims(X_train, axis=3)

        # Adversarial ground truths
        valid = -np.ones((batch_size, 1))
        fake = np.ones((batch_size, 1))

        for epoch in range(epochs):

            for _ in range(self.n_critic):

                # ---------------------
                #  Train Discriminator
                # ---------------------

                # Select a random batch of images
                idx = np.random.randint(0, X_train.shape[0], batch_size)
                imgs = X_train[idx]
                
                # Sample noise as generator input
                noise = np.random.normal(0, 1, (batch_size, self.latent_dim))
                #print(noise.shape)
                # Generate a batch of new images
                gen_imgs = self.generator.predict(noise)

                # Train the critic
                d_loss_real = self.critic.train_on_batch(imgs, valid)
                d_loss_fake = self.critic.train_on_batch(gen_imgs, fake)
                d_loss = 0.5 * np.add(d_loss_fake, d_loss_real)

                # Clip critic weights
                for l in self.critic.layers:
                    weights = l.get_weights()
                    weights = [np.clip(w, -self.clip_value, self.clip_value) for w in weights]
                    l.set_weights(weights)


            # ---------------------
            #  Train Generator
            # ---------------------

            g_loss = self.combined.train_on_batch(noise, valid)

            # Plot the progress
            print ("%d [D loss: %f] [G loss: %f]" % (epoch, 1 - d_loss[0], 1 - g_loss[0]))

            # If at save interval => save generated image samples
            #if epoch % sample_interval == 0:
            #    #self.sample_images(epoch)

    def sample_images(self, epoch):
        r, c = 5, 5
        noise = np.random.normal(0, 1, (r * c, self.latent_dim))
        gen_imgs = self.generator.predict(noise)

        # Rescale images 0 - 1
        gen_imgs = 0.5 * gen_imgs + 0.5

        fig, axs = plt.subplots(r, c)
        cnt = 0
        for i in range(r):
            for j in range(c):
                axs[i,j].imshow(gen_imgs[cnt, :,:,0], cmap='gray')
                axs[i,j].axis('off')
                cnt += 1
        fig.savefig("images/mnist_%d.png" % epoch)
        plt.close()

x_train_wgan, x_test_wgan, y_train_wgan, y_test_wgan = x_train, x_test, y_train, y_test
print(y_train_wgan.shape)

x_train_positive = [x_train_wgan[i] for i in range(x_train_wgan.shape[0]) if y_train_wgan[i] == 1]
X = np.array(x_train_positive).reshape(len(x_train_positive), len(x_train_positive[0]))
X = np.concatenate([X.copy() for i in range(20)])
data = X.reshape(X.shape[0], X.shape[1] ,1, 1)
Y_pos = np.array([1 for i in range(len(X))]).reshape(len(X), 1)
data.shape

len([x_test_wgan[i] for i in range(x_test_wgan.shape[0]) if y_test_wgan[i] == 1])

#if __name__ == '__main__':
#    wgan = WGAN()
#    wgan.train(epochs=10000, batch_size=16, sample_interval=50)

#if __name__ == '__main__':
#    wgan = WGAN()
#    wgan.train(epochs=7000, batch_size=16, sample_interval=50)

wgan = WGAN()

wgan.generator.load_weights('generator.h5')
ans = wgan.generator.predict(np.random.random([20000, 100]))
print(ans.shape)

ans = ans.reshape(ans.shape[0], ans.shape[1])
y_generated = np.array([1 for i in range(len(ans))]).reshape(len(ans), 1)

x_new_train = np.concatenate((x_train_wgan, ans))
x_new_train.shape
y_new_train = np.concatenate((y_train_wgan.reshape((len(y_train_wgan) , 1 )), y_generated))
y_new_train.shape

from sklearn.model_selection import train_test_split
x_new_train, junk1, y_new_train, junk2 = train_test_split(x_new_train, y_new_train, test_size = 0.01, random_state = 42)
y_new_train.shape

logreg = LogisticRegression()

logreg.fit(x_new_train, y_new_train)

y_pred = logreg.predict(x_test)
    
from sklearn.metrics import classification_report
from sklearn.metrics import accuracy_score
print('logreg:') 
print(classification_report(y_test, y_pred))

print(accuracy_score(y_test, y_pred))

from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import RandomForestClassifier
dectree = DecisionTreeClassifier()
randfor = RandomForestClassifier()

dectree.fit(x_new_train, y_new_train)

y_pred = dectree.predict(x_test)
print('Dec tree:')
print(classification_report(y_test, y_pred))
print(accuracy_score(y_test, y_pred))

#randfor.fit(x_new_train, y_new_train)
#
#y_pred = randfor.predict(x_test)
#print('Rand Forest:')
#print(classification_report(y_test, y_pred))
#print(accuracy_score(y_test, y_pred))


print('===plain===')
lr= LogisticRegression()

lr.fit(x_train, y_train)

y_pred = lr.predict(x_test)
    
print('logreg:')
print(classification_report(y_test, y_pred))
print(accuracy_score(y_test, y_pred))


dt = DecisionTreeClassifier()
dt.fit(x_train, y_train) 
y_pred = dt.predict(x_test)

print('dtree:')
print(classification_report(y_test, y_pred))
print(accuracy_score(y_test, y_pred))


#rf = RandomForestClassifier()
#rf.fit(x_train, y_train) 
#y_pred = rf.predict(x_test)
#
#print('rand forest:')
#print(classification_report(y_test, y_pred))
#print(accuracy_score(y_test, y_pred))

#X_tce_period = np.array(X_tce_period)
#
#X_curves_global = np.array(X_curves_global)
#X_curves_local = np.array(X_curves_local)
#X_tce_period = np.array(X_tce_period)
#Y = np.array(Y)

#np.savez("X_curves_global", X_curves_global, allow_pickle = True)
#np.savez("X_curves_local", X_curves_local, allow_pickle = True)
#np.savez("X_tce_period",X_tce_period, allow_pickle = True)
#np.savez("Y", Y, allow_pickle = True)
#import pickle
#pickle.dump(X_curves_global, open("X_curves_global.sav", "wb"))
#pickle.dump(X_curves_local, open("X_curves_local.sav", "wb"))
#pickle.dump(X_tce_period, open("X_tce_period.sav", "wb"))
#pickle.dump(Y, open("Y.sav", "wb"))

